import sys
import importlib

if __name__ == "__main__":
    controller_name = sys.argv[1]
    controller_method_name = sys.argv[2]

    class_to_call = getattr(
        importlib.import_module(
            "controllers."+controller_name
        ),
        controller_name.capitalize()
    )
    controller = class_to_call()
    method_to_call = getattr(controller, controller_method_name)
    try:
        method_to_call()
    except KeyboardInterrupt:
        print("Interrupted...")
