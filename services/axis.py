class Axis(object):
    axis_round = 2

    def get_created(self):
        return self.created

    def set_created(self, created):
        self.created = created

    def get_wheel_correction(self):
        try:
            return self.wheel_correction
        except AttributeError:
            return 0

    def set_wheel_correction(self, wheel_correction):
        self.wheel_correction = wheel_correction

    def get_accelerator_correction(self):
        try:
            return self.accelerator_correction
        except AttributeError:
            return 0

    def set_accelerator_correction(self, accelerator_correction):
        self.accelerator_correction = accelerator_correction

    def get_wheel(self):
        try:
            return self.wheel
        except AttributeError:
            return False

    def get_wheel_with_correction(self):
        return round(self.wheel + self.get_wheel_correction(), self.axis_round)

    def vjoy_fix(self, value):
        if value < 1:
            value = 1
        if value > 8000:
            value = 8000
        return value

    def get_wheel_vjoy(self):
        pygame_max = 2
        value_with_correction = round(
            pygame_max - self.get_wheel_with_correction(), self.axis_round
        )  # from left to right
        value_vjoy_hex = self.vjoy_fix(
            int(round(8000 * value_with_correction / pygame_max))
        )
        print(
            "< >"
            + "0x"
            + str(value_vjoy_hex)
            + "/0x8000 "
            + str(int(str(value_vjoy_hex), 16))
            + "/32768 "
            + str(value_with_correction)
            + "/2-0"
        )
        return int(str(value_vjoy_hex), 16)

    def set_wheel(self, wheel):
        self.wheel = round(wheel, self.axis_round)

    def get_clutch(self):
        return self.clutch

    def set_clutch(self, clutch):
        self.clutch = round(clutch, self.axis_round)

    def get_accelerator(self):
        try:
            return self.accelerator
        except AttributeError:
            return False

    def get_accelerator_with_correction(self):
        return round(
            self.accelerator + self.get_accelerator_correction(),
            self.axis_round
        )

    def get_accelerator_vjoy(self):
        value_with_correction = self.get_accelerator_with_correction()
        pygame_max = 2
        value_vjoy_hex = self.vjoy_fix(
            int(round(8000 * value_with_correction / pygame_max))
        )
        print(
            " ^ "
            + "0x"
            + str(value_vjoy_hex)
            + "/0x8000 "
            + str(int(str(value_vjoy_hex), 16))
            + "/32768 "
            + str(value_with_correction)
            + "/2-0"
        )
        return int(str(value_vjoy_hex), 16)

    def set_accelerator(self, accelerator):
        self.accelerator = round(accelerator, self.axis_round)

    def get_brake(self):
        try:
            if self.brake == 1.0:
                return 2.0
            else:
                return self.brake
        except AttributeError:
            return False

    def get_brake_vjoy(self):
        pygame_max = 2
        value = self.get_brake()
        value_vjoy_hex = self.vjoy_fix(
            int(round(8000 * value / pygame_max))
        )
        print(
            " v "
            + "0x"
            + str(value_vjoy_hex)
            + "/0x8000 "
            + str(int(str(value_vjoy_hex), 16))
            + "/32768 "
            + str(value)
            + "/2-0"
        )
        print("")
        return int(str(value_vjoy_hex), 16)

    def set_brake(self, brake):
        self.brake = round(brake, self.axis_round)

    def get_dict(self):
        return {
            "created": self.get_created(),
            "wheel": self.get_wheel(),
            "accelerator": self.get_accelerator(),
            "brake": self.get_brake(),
            "speed": self.get_speed()
        }

    def get_matrix(self):
        axis = []

        # 0
        if type(self.get_wheel()) == float:
            axis.append(self.get_wheel())
        # 1
        if type(self.get_accelerator()) == float:
            axis.append(self.get_accelerator())
        # 2
        if type(self.get_brake()) == float:
            axis.append(self.get_brake())
        # 3
        if type(self.get_speed()) == float:
            axis.append(self.get_speed())

        return axis

    def set_speed(self, speed):
        self.speed = float(round(speed))

    def get_speed(self, type="float"):
        try:
            if type == "array":
                print(" s " + str(self.speed))
                import numpy as np
                return np.asarray([self.speed])
            else:
                return self.speed
        except AttributeError:
            return False
