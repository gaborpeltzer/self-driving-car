import numpy as np
import keras
# import time


class DataGenerator(keras.utils.Sequence):
    def set_features(self, features):
        self.features = features

    def get_features(self):
        return self.features

    def set_labels(self, labels):
        self.labels = labels

    def get_labels(self):
        return self.labels

    def set_batch_size(self, batch_size):
        self.batch_size = batch_size

    def get_batch_size(self):
        return self.batch_size

    def set_input_shape(self, input_shape):
        self.input_shape = input_shape

    def get_input_shape(self):
        return self.input_shape

    def set_output_shape(self, output_shape):
        self.output_shape = output_shape

    def get_output_shape(self):
        return self.output_shape

    def set_shuffle(self, shuffle):
        self.shuffle = shuffle

    def get_shuffle(self):
        return self.shuffle

    def __len__(self):
        return int(
            np.floor(len(self.get_features()) / self.get_batch_size())
        )

    def __getitem__(self, index):
        indexes = self.indexes[
            index * self.get_batch_size():(index + 1) * self.get_batch_size()
        ]
        features_temp = [self.get_features()[k] for k in indexes]
        train_data = self.__data_generation(features_temp)
        return [train_data[0], train_data[1]], train_data[2]

    def index_setup(self):
        self.indexes = np.arange(len(self.get_features()))
        if self.get_shuffle() is True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, features_temp):
        # tic = time.perf_counter()
        images = np.empty((self.get_batch_size(), *self.get_input_shape()))
        speeds = np.empty((self.get_batch_size(), 1))
        labels = np.empty((self.get_batch_size(), *self.get_output_shape()))

        for index, feature in enumerate(features_temp):
            try:
                images[index, ] = np.load(
                    self.get_features_path() +
                    "/" +
                    str(feature.get_created()) +
                    "." +
                    self.get_feature_ext()
                )
            except ValueError:
                print("ValueError, file: " + str(feature.get_created()))
                exit()
            speeds[index] = feature.get_speed()

            labels[index] = self.get_labels()[str(feature.get_created())]
        # toc = time.perf_counter()
        # print(f"Data generated: {toc - tic:0.4f} seconds")
        return [images, speeds, labels]

    def set_crop(self, crop):
        self.crop = crop

    def get_crop(self):
        return self.crop

    def set_image_crop_size(self, image_crop_size):
        self.image_crop_size = image_crop_size

    def get_image_crop_size(self):
        return self.image_crop_size

    def set_resize(self, resize):
        self.resize = resize

    def get_resize(self):
        return self.resize

    def set_image_work_size(self, image_work_size):
        self.image_work_size = image_work_size

    def get_image_work_size(self):
        return self.image_work_size

    def set_features_path(self, features_path):
        self.features_path = features_path

    def get_features_path(self):
        return self.features_path

    def set_feature_ext(self, feature_ext):
        self.feature_ext = feature_ext

    def get_feature_ext(self):
        return self.feature_ext
