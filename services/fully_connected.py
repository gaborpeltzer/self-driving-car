from services.neural_network import NeuralNetwork
from keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, \
    Input, concatenate, LSTM, TimeDistributed
from keras.models import Model


class FullyConnected(NeuralNetwork):
    def create(self):
        input_image = Input(shape=self.get_input_shape())
        image = Conv2D(32, kernel_size=[3, 3], activation="relu")(input_image)
        image = MaxPooling2D(pool_size=[2, 2])(image)
        image = Conv2D(128, kernel_size=[3, 3], activation="relu")(image)
        image = MaxPooling2D(pool_size=[2, 2])(image)
        image = Conv2D(256, kernel_size=[3, 3], activation="relu")(image)
        image = TimeDistributed(Flatten())(image)
        image = LSTM(128)(image)

        input_speed = Input(shape=(1,))
        speed = Dense(1, activation="relu")(input_speed)

        model = concatenate([image, speed])
        model = Dense(self.get_output_shape()[-1])(model)

        self.model = Model(inputs=[input_image, input_speed], outputs=model)
