import mss
from PIL import Image as PilImage
from PIL import ImageDraw, ImageEnhance, ImageOps
import numpy as np


class Image(object):
    def set_source_size(self, size):
        self.source_size = size

    def get_source_size(self):
        return self.source_size

    def set_path(self, path):
        self.path = path

    def get_path(self):
        return self.path

    def load_from_file(self):
        try:
            self.set_pil_image(PilImage.open(self.get_path()))
        except FileNotFoundError:
            print("FileNotFoundError: " + self.get_path())
        try:
            self.get_pil_image().load()
        except OSError:
            print("OSError: " + self.get_path())
        self.set_source_size(self.get_pil_image().size)

    def crop(self, size):
        self.set_pil_image(self.get_pil_image().crop(size))

    def resize(self, size):
        self.set_pil_image(self.get_pil_image().resize(size))

    def save(self):
        self.get_pil_image().save(self.get_path())

    def save_one_channel(self):
        temp_matrix = self.get_matrix(1)
        image = PilImage.fromarray(temp_matrix[:, :, 0], 'L')
        image.save(self.get_path())

    def set_monitor_id(self, monitor_id):
        self.monitor_id = monitor_id

    def get_monitor_id(self):
        return self.monitor_id

    def take_screenshot(self):
        with mss.mss() as sct:
            sct_img = sct.grab(sct.monitors[self.get_monitor_id()])
            self.set_pil_image(
                PilImage.frombytes(
                    'RGB', sct_img.size, sct_img.bgra, 'raw', 'BGRX'
                )
            )

    def normalization(self, image_matrix):
        return image_matrix/255

    def get_four_dimension_matrix(self, image_channel_count, normalization):
        image_matrix = self.get_matrix(image_channel_count)
        if normalization is True:
            image_matrix = self.normalization(image_matrix)
        return self.convert_to_four_dimension(
            image_matrix
        )

    def get_matrix(self, image_channel_count):
        image_matrix = np.asarray(self.get_pil_image(), dtype=np.uint8)
        if(image_channel_count == 1):
            return self.convert_to_one_channel(image_matrix)
        else:
            return image_matrix

    def set_pil_image(self, pil_image):
        self.pil_image = pil_image

    def get_pil_image(self):
        return self.pil_image

    def convert_to_one_channel(self, image_array):
        return image_array[:, :, 2:3]

    def convert_to_green(self):
        self.set_pil_image(
            ImageOps.colorize(
                self.get_pil_image().convert("L"), black="black", white="green"
            )
        )

    def convert_to_four_dimension(self, three_dimension_image_array):
        return three_dimension_image_array[None, :, :, :]

    def overlay_transparent(self, over_image, box=None):
        enhancer = ImageEnhance.Brightness(over_image.get_pil_image())
        over_image.set_pil_image(enhancer.enhance(10))
        self.get_pil_image().paste(
            over_image.get_pil_image(),
            box=box,
            mask=over_image.get_pil_image().split()[1]
        )

    def overlay(self, over_image, box=None):
        self.get_pil_image().paste(
            over_image.get_pil_image(),
            box=box
        )

    def show(self):
        self.get_pil_image().show()

    def load_from_coordinates(self, all_coordinates):
        self.set_pil_image(
            PilImage.new('RGB', self.get_source_size(), (0, 0, 0))
        )
        draw = ImageDraw.Draw(self.get_pil_image())
        for item_coordinates in all_coordinates:
            draw.line(item_coordinates, fill="white", width=8)

    def get_file_name(self, only_name=False):
        if only_name is True:
            return int(self.get_path().split("/")[-1][:-4])
        else:
            return self.get_path().split("/")[-1]

    def save_matrix(self, save_path, image_channel_count, normalization):
        image_matrix = self.get_matrix(image_channel_count)
        if normalization is True:
            image_matrix = self.normalization(image_matrix)
        np.save(save_path, image_matrix)

    def is_file(self):
        from os import path
        return path.isfile(self.get_path())

    def is_file_matrix(self, matix_path, matrix_ext):
        from os import path
        return path.isfile(matix_path + "." + matrix_ext)

    def preprocess(
        self,
        image_crop_size,
        image_work_size,
        map_crop_size=False,
        map_work_size=False
    ):
        if map_crop_size is not False and map_work_size is not False:
            car_camera_screen_temp = Image()
            car_camera_screen_temp.set_pil_image(
                self.get_pil_image().copy()
            )
            car_camera_screen_temp.crop(map_crop_size)
            car_camera_screen_temp.resize(map_work_size)

        self.crop(image_crop_size)
        self.resize(image_work_size)

        if map_crop_size is not False and map_work_size is not False:
            self.overlay(car_camera_screen_temp)
