from services.axis_repository import AxisRepository
from services.image_repository import ImageRepository
import os


class Converter(object):
    def __init__(self, config):
        self.config = config

    def screenshot(self, test_only=False):
        os.makedirs(self.config.dataset_path_screens, exist_ok=True)
        os.makedirs(self.config.dataset_path_screens_matrix, exist_ok=True)

        axes = AxisRepository()
        axes.set_path(self.config.drive_control_log_path)
        axes.load_axes(limit=self.config.dataset_limit)

        car_camera_screens = ImageRepository()
        car_camera_screens.set_path(self.config.dataset_path_screens)
        car_camera_screens.set_creation_times(axes.get_creation_times(
            suffix=".jpg",
            limit=self.config.dataset_limit
        ))
        car_camera_screens.set_work_size(self.config.image_work_size)
        car_camera_screens.set_crop_size(self.config.image_crop_size)
        car_camera_screens.set_map_work_size(self.config.map_work_size)
        car_camera_screens.set_map_crop_size(self.config.map_crop_size)
        car_camera_screens.convert(
            self.config.dataset_path_screens_matrix,
            self.config.dataset_path_screens_matrix_ext,
            self.config.image_channel_count,
            self.config.dataset_limit,
            test_only=test_only
        )
        print("Finished")
