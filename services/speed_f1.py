import socket
# import time
import ctypes

# Game Options > Settings > Telemetry Settings: set UDP Telemetry to On
class PackedLittleEndianStructure(ctypes.LittleEndianStructure):
    _pack_ = 1

    def __repr__(self):
        fstr_list = []
        for field in self._fields_:
            fname = field[0]
            value = getattr(self, fname)
            if isinstance(
                value, (PackedLittleEndianStructure, int, float, bytes)
            ):
                vstr = repr(value)
            elif isinstance(value, ctypes.Array):
                vstr = "[{}]".format(", ".join(repr(e) for e in value))
            else:
                raise RuntimeError(
                    "Bad value {!r} of type {!r}".format(value, type(value))
                )
            fstr = f"{fname}={vstr}"
            fstr_list.append(fstr)
        return "{}({})".format(self.__class__.__name__, ", ".join(fstr_list))


class PacketHeader(PackedLittleEndianStructure):
    """The header for each of the UDP telemetry packets."""

    _fields_ = [
        ("packetFormat", ctypes.c_uint16),
        ("gameMajorVersion", ctypes.c_uint8),
        ("gameMinorVersion", ctypes.c_uint8),
        ("packetVersion", ctypes.c_uint8),
        ("packetId", ctypes.c_uint8),
        ("sessionUID", ctypes.c_uint64),
        ("sessionTime", ctypes.c_float),
        ("frameIdentifier", ctypes.c_uint32),
        ("playerCarIndex", ctypes.c_uint8),
        ("secondaryPlayerCarIndex", ctypes.c_uint8),
    ]


class CarMotionData_V1(PackedLittleEndianStructure):
    _fields_ = [
        ("worldPositionX", ctypes.c_float),
        ("worldPositionY", ctypes.c_float),
        ("worldPositionZ", ctypes.c_float),
        ("worldVelocityX", ctypes.c_float),
        ("worldVelocityY", ctypes.c_float),
        ("worldVelocityZ", ctypes.c_float),
        ("worldForwardDirX", ctypes.c_int16),
        ("worldForwardDirY", ctypes.c_int16),
        ("worldForwardDirZ", ctypes.c_int16),
        ("worldRightDirX", ctypes.c_int16),
        ("worldRightDirY", ctypes.c_int16),
        ("worldRightDirZ", ctypes.c_int16),
        ("gForceLateral", ctypes.c_float),
        ("gForceLongitudinal", ctypes.c_float),
        ("gForceVertical", ctypes.c_float),
        ("yaw", ctypes.c_float),
        ("pitch", ctypes.c_float),
        ("roll", ctypes.c_float),
    ]


class PacketMotionData_V1(PackedLittleEndianStructure):
    _fields_ = [
        ("header", PacketHeader),
        ("carMotionData", CarMotionData_V1 * 22),
        # Extra player car ONLY data
        ("suspensionPosition", ctypes.c_float * 4),
        ("suspensionVelocity", ctypes.c_float * 4),
        ("suspensionAcceleration", ctypes.c_float * 4),
        ("wheelSpeed", ctypes.c_float * 4),
        ("wheelSlip", ctypes.c_float * 4),
        ("localVelocityX", ctypes.c_float),
        ("localVelocityY", ctypes.c_float),
        ("localVelocityZ", ctypes.c_float),
        ("angularVelocityX", ctypes.c_float),
        ("angularVelocityY", ctypes.c_float),
        ("angularVelocityZ", ctypes.c_float),
        ("angularAccelerationX", ctypes.c_float),
        ("angularAccelerationY", ctypes.c_float),
        ("angularAccelerationZ", ctypes.c_float),
        ("frontWheelsAngle", ctypes.c_float),
    ]


HeaderFieldsToPacketType = {
    (2020, 1, 0): PacketMotionData_V1
    # (2020, 1, 1): PacketSessionData_V1,
    # (2020, 1, 2): PacketLapData_V1,
    # (2020, 1, 3): PacketEventData_V1,
    # (2020, 1, 4): PacketParticipantsData_V1,
    # (2020, 1, 5): PacketCarSetupData_V1,
    # (2020, 1, 6): PacketCarTelemetryData_V1,
    # (2020, 1, 7): PacketCarStatusData_V1,
    # (2020, 1, 8): PacketFinalClassificationData_V1,
    # (2020, 1, 9): PacketLobbyInfoData_V1,
}

udp_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
udp_socket.bind(("", 20777))


def gs():
    while True:
        key = ""
        actual_packet_size = 1
        expected_packet_size = 2

        packet = udp_socket.recv(2048)

        actual_packet_size = len(packet)
        header_size = ctypes.sizeof(PacketHeader)

        header = PacketHeader.from_buffer_copy(packet)
        key = (header.packetFormat, header.packetVersion, header.packetId)

        if key not in HeaderFieldsToPacketType:
            continue

        packet_type = HeaderFieldsToPacketType[key]
        expected_packet_size = ctypes.sizeof(packet_type)

        if actual_packet_size != expected_packet_size:
            continue

        a = packet_type.from_buffer_copy(packet)
        import statistics
        sp = statistics.mean([
            a.__getattribute__("wheelSpeed")[0],
            a.__getattribute__("wheelSpeed")[1],
            a.__getattribute__("wheelSpeed")[2],
            a.__getattribute__("wheelSpeed")[3]
        ])
        return sp
