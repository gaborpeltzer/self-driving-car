class DataSplitter():
    def set_features(self, features):
        self.features = features

    def get_features(self):
        return self.features

    def set_labels(self, labels):
        self.labels = labels

    def get_labels(self):
        return self.labels

    def get_features_train(self):
        return self.get_features()[:self.get_split_border()]

    def get_features_validation(self):
        return self.get_features()[self.get_split_border():]

    def get_split_border(self):
        return round(len(self.get_features()) * self.get_split_rate())

    def set_split_rate(self, split_rate):
        self.split_rate = split_rate

    def get_split_rate(self):
        return self.split_rate

    def get_labels_train(self):
        return dict(
            list(
                self.get_labels().items()
            )[:self.get_split_border()]
        )

    def get_labels_validation(self):
        return dict(
            list(
                self.get_labels().items()
            )[self.get_split_border():]
        )
