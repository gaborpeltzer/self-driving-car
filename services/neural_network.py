from keras.models import load_model
import os


class NeuralNetwork(object):
    def set_input_shape(self, input_shape):
        self.input_shape = input_shape

    def get_input_shape(self):
        return self.input_shape

    def set_output_shape(self, output_shape):
        self.output_shape = output_shape

    def get_output_shape(self):
        return self.output_shape

    def set_epochs(self, epochs):
        self.epochs = epochs

    def get_epochs(self):
        return self.epochs

    def set_loss(self, loss):
        self.loss = loss

    def get_loss(self):
        return self.loss

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer

    def get_optimizer(self):
        return self.optimizer

    def set_metrics(self, metrics):
        self.metrics = metrics

    def get_metrics(self):
        return self.metrics

    def compile(self):
        self.model.compile(
            optimizer=self.get_optimizer(),
            loss=self.get_loss(),
            metrics=self.get_metrics()
        )

    def prepare_multi_gpu(self):
        import tensorflow as tf
        strategy = tf.distribute.MirroredStrategy(
            cross_device_ops=tf.distribute.HierarchicalCopyAllReduce()
        )
        print('Number of devices: {}'.format(strategy.num_replicas_in_sync))
        with strategy.scope():
            self.create()
            self.compile()

    def fit(
        self,
        training_generator,
        validation_generator,
        early_stopping_patience=0
    ):
        callbacks = []
        if early_stopping_patience > 0:
            from keras.callbacks import EarlyStopping
            callbacks.append(
                EarlyStopping(
                    monitor='loss',
                    patience=early_stopping_patience
                )
            )

        if self.get_log_level() == "DEBUG":
            from keras.callbacks import TensorBoard
            callbacks.append(
                TensorBoard(
                    log_dir=self.get_log_dir(),
                    histogram_freq=1,
                    profile_batch=(10, 20)
                )
            )

        history = self.model.fit(
            training_generator,
            validation_data=validation_generator,
            epochs=self.get_epochs(),
            verbose=1,
            callbacks=callbacks,
            workers=os.cpu_count()
        )

        self.set_history(history)

    def set_log_level(self, log_level):
        self.log_level = log_level

    def get_log_level(self):
        return self.log_level

    def get_log_dir(self):
        import datetime
        return "logs/tensorboard/" + datetime.datetime.now().strftime(
            "%Y-%m-%d-%H-%M-%S"
        )

    def set_path(self, path):
        self.path = path

    def get_path(self):
        return self.path

    def load(self):
        self.model = load_model(self.get_path())

    def save(self):
        self.model.save(self.get_path())

    def is_saved(self):
        from os import path
        return path.isfile(self.get_path())

    def remove(self):
        if self.is_saved():
            import os
            os.remove(self.get_path())

    def summary(self):
        self.model.summary()

    def set_history(self, history):
        self.history = history

    def get_history(self):
        return self.history

    def predict(self, validation_data):
        return self.model.predict(validation_data, verbose=0)
