from services.image import Image
import os
import numpy as np
import threading


class ImageRepository(object):
    def get_crop_size(self):
        return self.crop_size

    def set_crop_size(self, size):
        self.crop_size = size

    def get_work_size(self):
        return self.work_size

    def set_work_size(self, size):
        self.work_size = size

    def get_map_crop_size(self):
        return self.map_crop_size

    def set_map_crop_size(self, size):
        self.map_crop_size = size

    def get_map_work_size(self):
        return self.map_work_size

    def set_map_work_size(self, size):
        self.map_work_size = size

    def set_images(self, images):
        self.images = images

    def get_images(self):
        return self.images

    def add(self, image):
        self.images.append(image)

    def get_matrices(self, channel_count):
        matrices = {}
        for image in self.get_images():
            if channel_count == 1:
                matrices[image.get_file_name(only_name=True)] = image. \
                    convert_to_one_channel(image.get_matrix())
            else:
                matrices.append(image.get_matrix())
        return matrices

    def set_path(self, path):
        self.path = path

    def get_path(self):
        return self.path

    def set_creation_times(self, creation_times):
        self.creation_times = creation_times

    def get_creation_times(self):
        return self.creation_times

    def convert_thread(
        self, screen_file_names_thread, crop, resize, index_thread,
        dataset_path_screen_matrix, matrix_ext,
        image_channel_count, test_only
    ):
        for file_name in screen_file_names_thread:
            car_camera_screen = Image()
            car_camera_screen.set_path(self.get_path() + "/" + file_name)

            matrix_path = dataset_path_screen_matrix + "/" + \
                str(car_camera_screen.get_file_name(only_name=True))

            if car_camera_screen.is_file_matrix(matrix_path, matrix_ext) \
                    is True:
                continue

            car_camera_screen.load_from_file()

            if test_only is True:
                continue

            car_camera_screen.preprocess(
                self.get_crop_size(),
                self.get_work_size(),
                self.get_map_crop_size(),
                self.get_map_work_size()
            )

            car_camera_screen.save_matrix(
                matrix_path,
                image_channel_count,
                True
            )

    def convert(self, dataset_path_screen_matrix, matrix_ext,
                image_channel_count, limit=0, crop=True, resize=True,
                test_only=False):
        screen_file_names = self.get_creation_times()

        if limit == 0:
            limit = len(screen_file_names)
        screen_file_names_limited = np.array(screen_file_names[:limit])
        del screen_file_names

        cpu_count = os.cpu_count()
        thread_rate = cpu_count * (limit // cpu_count)
        screen_file_names_splited = np.split(
            screen_file_names_limited[:thread_rate], cpu_count
        )
        screen_file_names_splited[-1] = np.append(
            screen_file_names_splited[-1],
            screen_file_names_limited[thread_rate:]
        )
        del screen_file_names_limited

        threads = []
        for index_thread, screen_file_names_thread in enumerate(
            screen_file_names_splited
        ):
            threads.append(
                threading.Thread(
                    target=self.convert_thread,
                    args=(
                        screen_file_names_thread,
                        crop,
                        resize,
                        index_thread,
                        dataset_path_screen_matrix,
                        matrix_ext,
                        image_channel_count,
                        test_only
                    )
                )
            )
        del screen_file_names_splited

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()
