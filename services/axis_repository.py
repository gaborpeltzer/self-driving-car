from services.axis import Axis
import json


class AxisRepository(object):
    def __init__(self):
        self.axes = []

    def get_first(self):
        return self.axes[0]

    def get_axes(self):
        return self.axes

    def get_matrices(self):
        matrices = {}
        for axis in self.get_axes():
            matrices[str(axis.get_created())] = axis.get_matrix()
        return matrices

    def get_creation_times(self, suffix="", limit=0):
        creation_times = []
        index = 1
        for axis in self.get_axes():
            creation_times.append(str(axis.get_created()) + suffix)
            if limit != 0 and index == limit:
                break
            index = index + 1
        return creation_times

    def get_axes_limited(self, limit=0):
        axes = []
        index = 1
        for axis in self.get_axes():
            axes.append(axis)
            if limit != 0 and index == limit:
                break
            index = index + 1
        return axes

    def set_path(self, path):
        self.path = path

    def get_path(self):
        return self.path

    def add(self, axis):
        self.axes.append(axis)

    def save(self):
        with open(self.get_path(), "a") as axis_file:
            for axis in self.get_axes():
                axis_file.write(json.dumps(axis.get_dict()))
                axis_file.write("\n")

    def load_axes(self, directions=[], limit=0):
        index = 1
        with open(self.get_path(), "r") as axis_file:
            for axis in axis_file:
                if axis == '\n':
                    continue

                axis_array = json.loads(axis)
                new_axis = Axis()

                new_axis.set_created(axis_array["created"])
                if "wheel" in directions:
                    new_axis.set_wheel(axis_array["wheel"])
                if "clutch" in directions:
                    new_axis.set_clutch(axis_array["clutch"])
                if "accelerator" in directions:
                    new_axis.set_accelerator(axis_array["accelerator"])
                if "brake" in directions:
                    new_axis.set_brake(axis_array["brake"])
                if "speed" in directions:
                    new_axis.set_speed(axis_array["speed"])

                self.add(new_axis)
                if limit != 0 and index == limit:
                    break
                index = index + 1

    def calculate_axis_quantity(self):
        matrices = {}
        for axis in self.get_axes():
            key = str(round(axis.get_wheel(), 2))
            if key in matrices:
                matrices[key] = matrices[key] + 1
            else:
                matrices[key] = 1

        result = {"keys": [], "values": []}
        for key in sorted(matrices):
            result["keys"].append(key)
            result["values"].append(matrices[key])

        return result
