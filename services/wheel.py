import sys
import contextlib
import pyvjoy
from services.axis_repository import AxisRepository
from services.axis import Axis
import time
import mmap
import struct
with contextlib.redirect_stdout(None):
    import pygame


class Wheel(object):
    def __init__(self, joystick_id):
        self.set_joystick_id(joystick_id)

    def set_joystick_id(self, joystick_id):
        self.joystick_id = joystick_id

    def get_joystick_id(self):
        return self.joystick_id

    def set_axes(self, axes):
        self.axes = axes

    def get_axes(self):
        return self.axes

    def set_joystick(self, joystick):
        self.joystick = joystick

    def get_joystick(self):
        return self.joystick

    def set_mode(self, mode):
        if mode == "read":
            pygame.init()
            pygame.joystick.init()

            self.set_joystick(pygame.joystick.Joystick(self.get_joystick_id()))
            print(self.get_joystick().get_name())
            sys.stdout.flush()
            self.get_joystick().init()
        elif mode == "write":
            self.set_joystick(pyvjoy.VJoyDevice(self.get_joystick_id()))

    def read_axes(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise KeyboardInterrupt

        axis = Axis()
        axis.set_wheel(self.get_joystick().get_axis(0)+1)
        axis.set_accelerator(self.get_joystick().get_axis(1)+1)
        axis.set_brake(self.get_joystick().get_axis(2)+1)
        axis.set_speed(self.get_speed())
        self.set_axes(AxisRepository())
        self.get_axes().add(axis)

    def drive(self, axis):
        # Overview
        #
        # Pygame value + 1 stored to drive log.
        #
        #                     | pyvjoy      |   DEC |    HEX | pygame | log
        # =================================================================
        # wheel left          | HID_USAGE_X | 32768 | 0x8000 |     -1 |   0
        # wheel center        | HID_USAGE_X | 16384 | 0x4000 |      0 |   1
        # wheel right         | HID_USAGE_X |     1 |    0x1 |      1 |   2
        # accelerator stay    | HID_USAGE_Y | 32768 | 0x8000 |      1 |   2
        # accelerator fwd100% | HID_USAGE_Y |     1 |    0x1 |     -1 |   0
        # brake stay          | HID_USAGE_Z | 32768 | 0x8000 |      1 |   2
        # brake bwd100%       | HID_USAGE_Z |     1 |    0x1 |     -1 |   0

        # Other functions
        # self.get_joystick().reset()
        # self.get_joystick().reset_buttons()
        # self.get_joystick().reset_povs()
        # self.get_joystick().set_button(6,1)
        # self.get_joystick().set_button(6,0)

        self.get_joystick().set_axis(
            pyvjoy.HID_USAGE_X,
            axis.get_wheel_vjoy()
        )
        self.get_joystick().set_axis(
            pyvjoy.HID_USAGE_Y, axis.get_accelerator_vjoy()
        )
        self.get_joystick().set_axis(pyvjoy.HID_USAGE_Z, axis.get_brake_vjoy())

    def calibrate(self, direction):
        time.sleep(3)
        if direction == "r":
            self.get_joystick().set_axis(pyvjoy.HID_USAGE_X, int(str(1), 16))
            time.sleep(3)
            self.get_joystick().set_axis(
                pyvjoy.HID_USAGE_X,
                int(str(4000), 16)
            )

        if direction == "t":
            self.get_joystick().set_axis(pyvjoy.HID_USAGE_Y, int(str(1), 16))
            time.sleep(3)
            self.get_joystick().set_axis(
                pyvjoy.HID_USAGE_Y,
                int(str(8000), 16)
            )

        if direction == "b":
            self.get_joystick().set_axis(pyvjoy.HID_USAGE_Z, int(str(4000), 16))
            time.sleep(3)
            self.get_joystick().set_axis(
                pyvjoy.HID_USAGE_Z,
                int(str(8000), 16)
            )

        if direction == "l":
            self.get_joystick().set_axis(
                pyvjoy.HID_USAGE_X, int(str(8000), 16)
            )
            time.sleep(3)
            self.get_joystick().set_axis(
                pyvjoy.HID_USAGE_X,
                int(str(4000), 16)
            )

    def get_speed(self):
        return self.get_speed_f1()

    def get_speed_gtav(self):
        buffer_size = 4
        data = mmap.mmap(
            0,
            buffer_size,
            tagname='gta_car_speed',
            access=mmap.ACCESS_READ
        )
        speed = data.read(buffer_size)
        x = struct.unpack('f', speed)
        return x[0]

    def get_speed_f1(self):
        from services.speed_f1 import gs
        s = gs()
        return float(s)
