import sys
import config
from services.image import Image
from services.wheel import Wheel
import time
import os


class Capture(object):
    def drive(self):
        time.sleep(3)

        os.makedirs(config.dataset_path_screens, exist_ok=True)
        os.makedirs(config.dataset_path_screens_matrix, exist_ok=True)

        wheel = Wheel(config.real_wheel_id)
        wheel.set_mode("read")
        car_camera_screen = Image()

        while True:
            created = int(round(time.time() * 1000))

            wheel.read_axes()
            wheel.get_axes().get_first().set_created(created)
            wheel.get_axes().set_path(config.drive_control_log_path)

            car_camera_screen.set_monitor_id(config.monitor_id)
            car_camera_screen.take_screenshot()
            car_camera_screen.set_path(
                config.dataset_path_screens + "/" + str(created) + ".jpg"
            )

            wheel.get_axes().save()
            car_camera_screen.save()

            # DEBUG
            car_camera_screen.set_path(config.real_time_drive_capture_path)
            car_camera_screen.preprocess(
                config.image_crop_size,
                config.image_work_size,
                config.map_crop_size,
                config.map_work_size
            )
            car_camera_screen.save()

            print(
                "< > "
                + str(wheel.get_axes().get_first().get_wheel())
                + "/0-2"
            )
            print(
                " ^  "
                + str(wheel.get_axes().get_first().get_accelerator())
                + "/2-0"
            )
            print(
                " v  "
                + str(wheel.get_axes().get_first().get_brake())
                + "/2-0"
            )
            print("")

            sys.stdout.flush()
            time.sleep(0.1)
