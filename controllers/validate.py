import config


class Validate(object):
    def drive(self):
        from services.converter import Converter
        converter = Converter(config)
        converter.screenshot(test_only=True)

    def controller(self):
        import pygame

        pygame.init()
        pygame.joystick.init()
        joystick_count = pygame.joystick.get_count()

        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            print(
                "id: " +
                str(joystick.get_id()) +
                " name: " +
                str(joystick.get_name())
            )

        pygame.quit()

    def label_count(self):
        from services.axis_repository import AxisRepository
        import matplotlib.pyplot as plt

        axes = AxisRepository()
        axes.set_path(config.drive_control_log_path)
        axes.load_axes(directions=["wheel"], limit=config.dataset_limit)
        axis_quantity = axes.calculate_axis_quantity()

        plt.bar(
            range(len(axis_quantity["values"])),
            axis_quantity["values"],
            align='center'
        )
        plt.xticks(range(len(axis_quantity["keys"])), axis_quantity["keys"])
        plt.show()
