import config
from services.wheel import Wheel


class Calibrate(object):
    def wheel(self):
        wheel = Wheel(config.virtual_wheel_id)
        wheel.set_mode("write")
        while True:
            cmd = input("Axis: ")
            if cmd == "q":
                break
            wheel.calibrate(cmd)
