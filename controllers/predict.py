import sys
import config
from services.image import Image
from services.neural_network import NeuralNetwork
from services.axis import Axis
import os
from services.wheel import Wheel


class Predict(object):
    def drive(self):
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        cnn_drive = NeuralNetwork()
        cnn_drive.set_path(config.model_path_drive)
        cnn_drive.load()

        wheel = Wheel(config.virtual_wheel_id)
        wheel.set_mode("write")

        axis = Axis()
        axis.set_wheel(0)
        axis.set_brake(0)
        axis.set_accelerator(2)
        wheel.drive(axis)

        while True:
            car_camera_screen = Image()
            car_camera_screen.set_monitor_id(config.monitor_id)
            car_camera_screen.take_screenshot()
            car_camera_screen.preprocess(
                config.image_crop_size,
                config.image_work_size,
                config.map_crop_size,
                config.map_work_size
            )

            # DEBUG
            car_camera_screen.set_path(config.real_time_drive_prediction_path)
            car_camera_screen.save_one_channel()

            axis = Axis()
            axis.set_speed(wheel.get_speed())

            prediction_drive = cnn_drive.predict(
                [
                    car_camera_screen.get_four_dimension_matrix(
                        config.image_channel_count,
                        True
                    ),
                    axis.get_speed(type="array")
                ]
            )

            axis.set_wheel_correction(config.wheel_correction)
            axis.set_wheel(prediction_drive[0][0])
            axis.set_accelerator_correction(config.accelerator_correction)
            axis.set_accelerator(prediction_drive[0][1])
            axis.set_brake(prediction_drive[0][2])

            wheel.drive(axis)
            sys.stdout.flush()
