import config
from services.image_repository import ImageRepository
from services.fully_connected import FullyConnected
from services.plot import Plot
from services.axis_repository import AxisRepository
from services.data_splitter import DataSplitter
from services.data_generator import DataGenerator
import time


class Train(object):
    def drive(self):
        tic = time.perf_counter()
        screens = ImageRepository()
        screens.set_path(config.dataset_path_screens_matrix)

        features = AxisRepository()
        features.set_path(config.drive_control_log_path)
        features.load_axes(config.feature_keys, config.dataset_limit)

        labels = AxisRepository()
        labels.set_path(config.drive_control_log_path)
        labels.load_axes(config.label_keys, config.dataset_limit)

        cnn = FullyConnected()
        cnn.set_path(config.model_path_drive)
        cnn.set_epochs(config.epochs_drive)
        if config.train_continue is True and cnn.is_saved() is True:
            cnn.load()
        else:
            cnn.set_input_shape(config.fully_connected_input_shape)
            cnn.set_output_shape(config.fully_connected_output_shape)
            cnn.set_optimizer("adam")
            cnn.set_loss("mean_squared_error")
            cnn.set_metrics(["accuracy"])
            if config.multi_gpu_support is True:
                cnn.prepare_multi_gpu()
            else:
                cnn.create()
                cnn.compile()
        cnn.remove()

        data_splitter = DataSplitter()
        data_splitter.set_split_rate(config.train_validation_rate)
        data_splitter.set_features(features.get_axes_limited())
        data_splitter.set_labels(labels.get_matrices())

        training_generator = DataGenerator()
        training_generator.set_crop(True)
        training_generator.set_image_crop_size(config.image_crop_size)
        training_generator.set_resize(True)
        training_generator.set_image_work_size(config.image_work_size)
        training_generator.set_features_path(
            config.dataset_path_screens_matrix
        )
        training_generator.set_features(
            data_splitter.get_features_train()
        )
        training_generator.set_feature_ext(
            config.dataset_path_screens_matrix_ext
        )
        training_generator.set_labels(data_splitter.get_labels_train())
        training_generator.set_batch_size(config.batch_size_drive)
        training_generator.set_input_shape(config.fully_connected_input_shape)
        training_generator.set_output_shape(
            config.fully_connected_output_shape
        )
        training_generator.set_shuffle(False)
        training_generator.index_setup()

        validation_generator = DataGenerator()
        validation_generator.set_crop(True)
        validation_generator.set_image_crop_size(config.image_crop_size)
        validation_generator.set_resize(True)
        validation_generator.set_image_work_size(config.image_work_size)
        validation_generator.set_features_path(
            config.dataset_path_screens_matrix
        )
        validation_generator.set_features(
            data_splitter.get_features_validation()
        )
        validation_generator.set_feature_ext(
            config.dataset_path_screens_matrix_ext
        )
        validation_generator.set_labels(data_splitter.get_labels_validation())
        validation_generator.set_batch_size(config.batch_size_drive)
        validation_generator.set_input_shape(
            config.fully_connected_input_shape
        )
        validation_generator.set_output_shape(
            config.fully_connected_output_shape
        )
        validation_generator.set_shuffle(False)
        validation_generator.index_setup()

        cnn.set_log_level(config.log_level)

        cnn.fit(
            training_generator,
            validation_generator,
            config.early_stopping_patience
        )
        cnn.save()

        plot = Plot()
        plot.render(cnn.get_history(), config.last_train_history_path_drive)
        toc = time.perf_counter()
        print(f"Data generated: {toc - tic:0.4f} seconds")
