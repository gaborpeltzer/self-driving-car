# Self driving car based on neural network

## Overview

[![Self driving car](http://i3.ytimg.com/vi/RLM_lOWF_zc/hqdefault.jpg)](https://youtu.be/RLM_lOWF_zc)

## Requirements

- gta v https://store.steampowered.com/app/271590/Grand_Theft_Auto_V/
- steering wheel or game controller
- script hook v http://www.dev-c.com/gtav/scripthookv/
- manual transmission & steering wheel support https://www.gta5-mods.com/scripts/manual-transmission-ikt
- vjoy http://vjoystick.sourceforge.net/site/index.php/download-a-install/download
- wsl2
- python3.8.10
- pip3
- pip3 packages (pip3 install -r requirements.txt)
- copy config.sample.py to config.py
- copy dependencies/NativeSpeedoToMemory.asi to GTA folder
- add "[full-path]/self-driving-car" to PATH and make sure that the sdc file is runnable

## Use

1. sdc validate controller
2. sdc capture drive [dataset-name]
3. sdc validate drive [dataset-name]
4. sdc validate label_count [dataset-name]
5. sdc convert screenshot [dataset-name]
6. docker build -t sdc-trainer .
7. docker run --rm -v [full-path]\self-driving-car:/project --gpus all -it sdc-trainer /project/sdc train drive [dataset-name]
8. tensorboard --logdir logs/tensorboard
9. sdc calibrate wheel  
  - press "[" key in english keyboard for open manual transmission menu  
  - steering wheel / analog input setup  
  - press "r" for configure steering to right  
  - press "l" for configure steering to left  
  - press "t" for configure throttle  
  - press "b" for configure brake
10. sdc predict drive [dataset-name]

## Steering wheel hot keys

Start capture:  
cmd.exe /c "C:\Program Files\Git\git-bash.exe" -c 'sdc capture drive [dataset-name]'  
Stop capture:  
cmd.exe /c TASKKILL /F /IM python.exe

## Workflow

![workflow](/datas/gta-gps/ai-flow.jpg)

## Steering wheel occurrence

![self driving car axis range](/datas/gta-gps/steering_wheel_occurrence.jpg)

## Contact

https://twitter.com/gaborpeltzer
