FROM tensorflow/tensorflow:2.13.0-gpu

RUN pip install --no-cache-dir --upgrade pip
COPY ./requirements.txt /requirements.txt
RUN pip install --no-cache-dir -r /requirements.txt