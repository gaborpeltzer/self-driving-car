import sys
import os

try:
    dataset_name = sys.argv[3]
except IndexError:
    dataset_name = ""

# 0 = all
dataset_limit = 0
train_continue = False
early_stopping_patience = 4
train_validation_rate = 0.70
multi_gpu_support = False

monitor_id = 1
image_crop_size = (408, 515, 1920, 725)  # 1512x210
image_work_size = (360, 50)
map_crop_size = (90, 878, 240, 1028)
map_work_size = (50, 50)

feature_keys = ["speed"]
label_keys = ["wheel", "accelerator", "brake"]

# 3 = RGB
# 1 = B/W
image_channel_count = 1

fully_connected_input_shape = (
    image_work_size[1], image_work_size[0], image_channel_count
)
fully_connected_output_shape = (len(label_keys),)

dataset_path_screens = "datas/" + dataset_name + "/screenshots"
dataset_path_screens_matrix = "datas/" + dataset_name \
    + "/screenshots_preprocessed"
dataset_path_screens_matrix_ext = "npy"
model_path_drive = "datas/" + dataset_name + "/drive.h5" #.keras in the future
drive_control_log_path = "datas/" + dataset_name + "/drive_control.log"
real_time_drive_prediction_path = "datas/" + dataset_name \
    + "/real_time_drive_prediction.jpg"
real_time_drive_capture_path = "datas/" + dataset_name \
    + "/real_time_drive_capture.jpg"
last_train_history_path_drive = "datas/" + dataset_name \
    + "/last_train_history_drive.png"

wheel_correction = 0
accelerator_correction = 0

virtual_wheel_id = 1
real_wheel_id = 0

batch_size_drive = 64
epochs_drive = 200

# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"
log_level = "DEBUG"
